import React from 'react'
import Css from './title.scss'

export default (props) => (
    <h1 className={Css.title}>
        {props.children}
    </h1>
)