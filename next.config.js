const fs = require('fs')
const mkdirp = require('mkdirp')
const getDirName = require('path').dirname
const writeFileSync = function writeFileSync(path, contents, cb) {
  mkdirp(getDirName(path), function (err) {
    if (err) return cb(err)
    fs.writeFileSync(path, contents, cb);
  })
}
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  webpack: (config) => {
    config.plugins.push(new ExtractTextPlugin({
      filename: (getPath) => {
        /* Use ExtractTextPlugin to merge many SCSS file into one bundle CSS */
        const paths = getPath('./static/[name]')
        return paths.replace('.js', '.bundle.css')
      },
      allChunks: true,
      disable: false
    }));

    config.module.rules.push(
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'skeleton-loader',
              options: {
                procedure: function (content) {
                  /* This function is used to prepare SERVER-SIDE css module */

                  _idx = content.indexOf('exports.locals');
                  _r = eval(content.substr(_idx));

                  const p = this._module.resource.split('\\').pop().split('/')
                  const rawFileName = p.pop()
                  const rawFolderName = p.length > 1 ? p.pop() : ""
                  const targetName = rawFolderName === "" ? config.output.path + "/dist/" + rawFileName
                    : config.output.path + "/dist/" + rawFolderName + "/" + rawFileName

                  const jsObj = 'module.exports = ' + JSON.stringify(_r);
                  writeFileSync(targetName, jsObj, 'utf8')

                  /* Don't modify the content */
                  return content
                }
              }
            },
            {
              /* This css-loader is used to prepare CLIENT-SIDE css module */
              loader: 'css-loader',
              options: {
                modules: true,
                // change to false when development mode
                minimize: process.env.NODE_ENV === 'production' ? true : false,
                localIdentName: process.env.NODE_ENV === 'production' ? '[name]__[local]__[sha256:hash:8]' : '[name]-[local]',
                importLoaders: 1
              }
            },
            {
              loader: 'postcss-loader'
            },
            {
              /* sass compiler first */
              loader: 'sass-loader'
            }
          ]
        })
      }
    )
    return config
  }
}
