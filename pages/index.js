import React from 'react'
import Link from 'next/link'
import Head from 'next/head'

/* self scss MUST import before any component you used */
import Css from './index.scss'

import Title from '../components/title'


export default () => (
    <div>
        <Head>
            <link href="/bundles/pages/index.bundle.css" rel="stylesheet"/>            
        </Head>
        <Title>
            <span className={Css.tony}> Hello World </span>
        </Title>
    </div>
)