import React from 'react'
import Head from 'next/head'

import Css from './samplepage.scss'

export default () => (
    <div>
        <Head>
            <link href="/bundles/pages/samplepage.html.bundle.css" rel="stylesheet" />
        </Head>
        <div className={Css.sample}> Hello Sample </div>
    </div>
)