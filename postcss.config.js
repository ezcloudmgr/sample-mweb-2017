module.exports = {
    plugins: [
        // autoprefixer already include in cssnext
        // http://cssnext.io for detail feature
        require('postcss-cssnext')(),
    ]
}